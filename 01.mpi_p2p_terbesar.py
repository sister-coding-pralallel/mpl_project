# import mpi4py
from mpi4py import MPI
import numpy

# buat COMM
comm = MPI.COMM_WORLD

# dapatkan rank proses
rank = comm.Get_rank()

# dapatkan total proses berjalan
size = comm.Get_size()

# jika saya rank terbesar maka saya akan mengirimkan pesan ke proses yang mempunyai rank 0 s.d rank terbesar-1
if rank == (size-1) :
	idata = 0
	while idata < (size-1):
    		comm.send('Misi numpang lewat', dest=idata)
		idata = idata+1
	

# jika saya bukan rank terbesar maka saya akan menerima pesan yang berasal dari proses dengan rank terbesar
else:
    idata = comm.recv(source=size-1)
    print(idata,rank)	
