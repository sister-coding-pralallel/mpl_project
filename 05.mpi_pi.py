# import mpi4py
from mpi4py import MPI

# buat fungsi dekomposisi bernama local_loop 
# local_loop akan menghitung setiap bagiannya
# gunakan 4/(1+x^2), perhatikan batas awal dan akhir untuk dekomposisi
# misalkan size = 4 maka proses 0 menghitung 0-25, proses 1 menghitung 26-50, dst
def local_loop(num_steps,begin,end):
    step = 1.0/num_steps
    jmlah = 0
    # 4/(1+x^2)
    for i in range(begin,end):
        jmlah = jmlah + 4/(1+step**2)
    print (jmlah)
    return jmlah  

# fungsi Pi
def Pi(num_steps):
    
    # buat COMM
    comm = MPI.COMM_WORLD
    
    # dapatkan rank proses
    rank = comm.Get_rank()
    
    # dapatkan total proses berjalan
    size = comm.Get_size()
    
    # buat variabel baru yang merupakan num_steps/total proses
    var = num_steps/size
    
    # cari local_sum
    if rank==0:
        local_sum = local_loop(num_steps,0,25)
    else:
        begin = (rank*25)+1
        end = (rank+1)*25
        local_sum = local_loop(num_steps,begin,end)
    # local_sum merupakan hasil dari memanggil fungsi local_loop
    
    # lakukan penjumlahan dari local_sum proses-proses yang ada ke proses 0
    # bisa digunakan reduce atau p2p sum
    if rank == 0:
        listdata = [local_sum]
        for i in range(size-1):
            data = comm.recv(source=(i+1))
            listdata.append(data)
        jml = sum(listdata)
    else:
        comm.send(local_sum,dest=0)
    
    # jika saya proses dengan rank 0  maka tampilkan hasilnya
    if rank == 0:
        pi = jml / num_steps
        print (pi)
    
# panggil fungsi utama    
if __name__ == '__main__':
    Pi(10000)
