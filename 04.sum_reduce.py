# import mpi4py
from mpi4py import MPI

# import library random untuk generate angka integer secara random
import numpy as np

# buat COMM
comm = MPI.COMM_WORLD

# dapatkan rank proses
rank = comm.Get_rank()

# dapatkan total proses berjalan
size = comm.Get_size()

# generate angka integer secara random untuk setiap proses
angka 		= np.array(rank,'d')
value_sum      = np.array(0.0,'d')
print(' Rank: ',rank, ' value = ', angka)

# lakukam penjumlahan dengan teknik reduce, root reduce adalah proses dengan rank 0
comm.Reduce(angka, value_sum, op=MPI.SUM, root=0)

# jika saya proses dengan rank 0 maka saya akan menampilkan hasilnya
if rank== 0 :
	print(' Rank 0: value_sum =    ',value_sum)

